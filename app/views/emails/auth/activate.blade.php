Hello {{ $username }}, <br><br>

Please use the link below to activate your account.<br><br>

---<br>
{{ $link }}<br>
---